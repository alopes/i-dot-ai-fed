/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      colors: {
        "idotai-blue": "#3678d6",
      },
      fontFamily: {
        pangolin: ["Pangolin", "sans-serif"],
      },
    },
  },
  plugins: [],
};
