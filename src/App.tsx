// import logo from "./logo.svg";
import "./App.css";
import Test from "./components/Test";

function App() {
  return (
    <div className="font-pangolin">
      <Test />
    </div>
  );
}

export default App;
