import { useState, useEffect } from "react";
import { Root } from "./user.type";

export const useUsers = () => {
  const [loading, setLoading] = useState(false);
  const [didError, setDidError] = useState(false);
  const [users, setUsers] = useState<
    { id: number; name: string; city: string }[]
  >([]);

  const fetchData = async () => {
    setLoading(true);
    try {
      const result = await fetch("https://jsonplaceholder.typicode.com/users")
        .then((r) => r.json())
        .then((r: Root) => {
          return r.map((user) => ({
            id: user.id,
            name: user.name,
            city: user.address.city,
          }));
        });

      setUsers(result);
    } catch (error) {
      setDidError(true);
    }
    setLoading(false);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const refetch = () => {
    fetchData();
  };

  return {
    loading,
    didError,
    refetch,
    users,
  };
};
