import { useState, useEffect } from "react";
import { Root } from "./book.type";

export const useBooks = () => {
  const [loading, setLoading] = useState(false);
  const [didError, setDidError] = useState(false);
  const [books, setBooks] = useState<
    { id: string; title: string; thumbnailUrl?: string }[]
  >([]);

  const fetchData = async () => {
    setLoading(true);
    try {
      const result = await fetch(
        "https://www.googleapis.com/books/v1/volumes?q=flowers"
      )
        .then((r) => r.json())
        .then((r: Root) => {
          return r.items
            .map((book) => ({
              id: book.id,
              title: book.volumeInfo.title,
              thumbnailUrl: book.volumeInfo.imageLinks?.thumbnail,
            }))
            .filter((book) => !!book.thumbnailUrl);
        });

      setBooks(result);
    } catch (error) {
      setDidError(true);
    }
    setLoading(false);
  };

  useEffect(() => {
    fetchData();
  }, []);

  return {
    loading,
    didError,
    books,
  };
};
