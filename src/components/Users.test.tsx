import { render, screen, fireEvent } from "@testing-library/react";
import Users from "./Users";
import * as hooks from "../hooks/useUsers";

describe("<Users/>", () => {
  it("starts in a loading state", () => {
    render(<Users />);
    const linkElement = screen.getByText(/Loading users/i);
    expect(linkElement).toBeInTheDocument();
  });

  it("renders the list of users", () => {
    jest.spyOn(hooks, "useUsers").mockImplementation(() => ({
      users: [{ id: 1, name: "John", city: "New York" }],
      loading: false,
      didError: false,
      refetch: jest.fn(),
    }));

    render(<Users />);
    expect(screen.getByText(/John/i)).toBeInTheDocument();
    expect(screen.getByText(/New York/i)).toBeInTheDocument();
  });

  it("shows an error state", () => {
    jest.spyOn(hooks, "useUsers").mockImplementation(() => ({
      users: [],
      loading: false,
      didError: true,
      refetch: jest.fn(),
    }));

    render(<Users />);
    expect(screen.getByText(/Something went wrong/i)).toBeInTheDocument();
  });

  it("fetches data on demand", () => {
    const spy = jest.fn();
    jest.spyOn(hooks, "useUsers").mockImplementation(() => ({
      users: [],
      loading: false,
      didError: false,
      refetch: spy,
    }));

    render(<Users />);
    fireEvent.click(screen.getByTestId("refetch"));
    expect(spy).toHaveBeenCalled();
  });
});
