import settings from "../images/settings.svg";

const Header = () => {
  return (
    <header className="flex justify-between w-full lg:max-w-3xl mx-auto pt-6 px-2 items-center">
      <h1 className="text-3xl font-bold">Photo book</h1>
      <button
        aria-label="settings"
        onClick={() => {
          alert("settings menu");
        }}
      >
        <img src={settings} className="w-10 h-10" alt="logo" />
      </button>
    </header>
  );
};

export default Header;
