import facebook from "../images/facebook.svg";
import twitter from "../images/twitter.svg";

const Footer = () => {
  return (
    <footer className="bg-idotai-blue">
      <div className=" lg:max-w-3xl mx-auto space-y-4 py-8">
        <span className="text-white justify-center flex text-2xl">
          Get in touch
        </span>
        <div className="flex justify-center gap-4">
          <img src={facebook} className="w-14 h-14" alt="logo" />
          <img src={twitter} className="w-14 h-14" alt="logo" />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
