import React from "react";

const Terms = () => {
  return (
    <section className="space-y-4">
      <h2 className="text-2xl font-semibold">Terms</h2>
      <ul className="list-disc list-inside pl-4">
        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
        <li>Lorem ipsum dolor sit amet consectetur adipisicing elit.</li>
      </ul>
    </section>
  );
};

export default Terms;
