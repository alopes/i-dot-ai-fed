import { useUsers } from "../hooks/useUsers";
import refresh from "../images/refresh.svg";
import { classNames } from "../utils";

const Users = () => {
  const { users, loading, didError, refetch } = useUsers();

  return (
    <section className="p-4 bg-[#4da17f]">
      <div className="flex justify-between py-4">
        <h2 className="text-2xl text-white font-semibold">Latest users</h2>
        <button
          data-testid="refetch"
          aria-label="refresh user list"
          onClick={() => {
            refetch();
          }}
        >
          <img
            src={refresh}
            alt=""
            className={classNames(
              loading ? "animate-spin" : "",
              "w-10 h-10 bg-idotai-blue p-2 rounded-full"
            )}
          />
        </button>
      </div>

      {loading && <div>Loading users</div>}
      {didError && <div>Something went wrong</div>}

      {!loading && !!users.length && (
        <ul className="px-2 bg-[#84bda7] divide-y divide-white max-h-96 overflow-x-scroll">
          {users.map((user) => (
            <li
              key={user.id}
              className="flex justify-between items-center px-2 py-4 text-white"
            >
              <span>{user.name}</span>
              <span>{user.city}</span>
            </li>
          ))}
        </ul>
      )}
    </section>
  );
};

export default Users;
