import Books from "./Books";
import Footer from "./Footer";
import Header from "./Header";
import PhotoBook from "./PhotoBook";
import Terms from "./Terms";
import Users from "./Users";

const Test = () => {
  return (
    <>
      <Header />
      <main className="w-full lg:max-w-3xl mx-auto space-y-4 py-4 px-2">
        <PhotoBook />
        <Users />
        <Books />
        <Terms />
      </main>
      <Footer />
    </>
  );
};

export default Test;
