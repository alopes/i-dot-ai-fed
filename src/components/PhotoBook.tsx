import React from "react";

const PhotoBook = () => {
  return (
    <section className="text-white bg-black p-4 pt-6 space-y-16">
      <div className="space-y-4">
        <h1 className="text-2xl">
          Read your latest photo books and get inspired!
        </h1>
        <p>
          New photos available in
          <span className="countdown-border">4</span>d
          <span className="countdown-border">23</span>h
        </p>
      </div>

      <div className="w-full p-4 space-y-4 bg-white/25">
        <p className="text-xl">Don't miss out on the magic</p>
        <button className="p-4 text-black w-full text-xl bg-idotai-blue hover:bg-[#2b63b0]">
          Join Now
        </button>
      </div>
    </section>
  );
};

export default PhotoBook;
