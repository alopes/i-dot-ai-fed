import { useBooks } from "../hooks/useBooks";
import { classNames } from "../utils";

const Books = () => {
  const { books, loading, didError } = useBooks();

  return (
    <section className="p-4 bg-[#f8d161]">
      <div className="flex justify-between py-4">
        <h2 className="text-2xl text-white font-semibold">Latest books</h2>
      </div>

      {loading && <div>Loading books</div>}
      {didError && <div>Something went wrong</div>}

      {!loading && !!books.length && (
        <div className="grid gap-4 grid-cols-3 grid-rows-3">
          {books.map((book, index) => (
            <img
              key={book.id}
              className={classNames(
                index === 0
                  ? "row-start-1 row-end-3 col-start-1 col-end-3 md:row-end-2 md:col-end-2"
                  : "",
                "w-full h-full"
              )}
              src={book.thumbnailUrl}
              alt={book.title}
            />
          ))}
        </div>
      )}
    </section>
  );
};

export default Books;
